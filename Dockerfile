# OpenCVContainer

# Ultima Ratio Regis
# Author Mauricio Costa Pinheiro - 2015 Nov

FROM ubuntu:14.04

MAINTAINER Mauricio Costa Pinheiro<mauricio@ur2.com.br>

# Removing any pre-installed ffmpeg and x264
RUN apt-get -y -qq remove ffmpeg
#RUN apt-get -y -qq remove x264
#RUN apt-get -y -qq remove libx264-dev

# Update
RUN apt-get update
RUN apt-get -y upgrade

# Install git
RUN apt-get -y -qq install git

# Installing Dependenices
RUN apt-get -y -qq install cmake
RUN apt-get -y -qq install checkinstall
RUN apt-get -y -qq install libopencv-dev build-essential pkg-config yasm
RUN apt-get -y -qq install libjpeg-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev libxine-dev
RUN apt-get -y -qq install libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev 
RUN apt-get -y -qq install python-dev python-numpy
RUN apt-get -y -qq install libtbb-dev 
RUN apt-get -y -qq install libqt4-dev libgtk2.0-dev
RUN apt-get -y -qq install libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev
RUN apt-get -y -qq install x264 v4l-utils

#RUN echo "deb http://archive.ubuntu.com/ubuntu/ trusty multiverse" >> /etc/apt/sources.list
#RUN echo "deb-src http://archive.ubuntu.com/ubuntu/ trusty multiverse" >> /etc/apt/sources.list
#RUN echo "deb http://archive.ubuntu.com/ubuntu/ trusty-updates multiverse" >> /etc/apt/sources.list
#RUN echo "deb-src http://archive.ubuntu.com/ubuntu/ trusty-updates multiverse" >> /etc/apt/sources.list
#RUN apt-get update
#RUN apt-get -y -qq install libfaac-dev
#RUN apt-get -y -qq install ffmpeg

RUN mkdir /temp
WORKDIR /temp

RUN git clone https://github.com/Itseez/opencv.git

# Build
RUN mkdir /temp/opencv/build
WORKDIR /temp/opencv/build
RUN cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=OFF -D INSTALL_PYTHON_EXAMPLES=OFF -D BUILD_EXAMPLES=OFF -D WITH_QT=OFF -D WITH_OPENGL=ON ..
RUN make -j2
RUN checkinstall
RUN sh -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
RUN ldconfig

WORKDIR /
RUN rm -R /temp
